package au.com.timmutton.krazykats.presentation.home

import au.com.timmutton.krazykats.presentation.home.HomeScreen.given
import au.com.timmutton.krazykats.presentation.home.HomeScreen.then
import au.com.timmutton.krazykats.presentation.home.HomeScreen.whenever
import au.com.timmutton.krazykats.test.BaseTest
import org.junit.Test

class HomeTest: BaseTest() {
    @Test
    fun givenLaunch_andScheduleSuccess_thenDisplayCats() {
        given {
            iHaveNetworkAccess() // and
            iAmOnHomeScreen()
        }

        then {
            catsAreDisplayed()
        }
    }

    @Test
    fun givenLaunch_andScheduleError_thenDisplayError() {
        given {
            iDoNotHaveNetworkAccess() // and
            iAmOnHomeScreen()
        }

        then {
            errorIsDisplayed()
        }
    }

    @Test
    fun givenLaunch_andScheduleError_whenIRetry_thenDisplayCats() {
        given {
            iDoNotHaveNetworkAccess() // and
            iAmOnHomeScreen()
        }

        whenever {
            iHaveNetworkAccess()
            iRetryGettingSchedule()
        }

        then {
            catsAreDisplayed()
        }
    }
}