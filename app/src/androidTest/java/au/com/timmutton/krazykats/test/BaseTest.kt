package au.com.timmutton.krazykats.test

import android.support.annotation.CallSuper
import android.support.test.runner.AndroidJUnit4
import org.junit.After
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
abstract class BaseTest {
    @After
    @CallSuper
    open fun after() {
        TestRunner.dispatcher.reset()
    }
}