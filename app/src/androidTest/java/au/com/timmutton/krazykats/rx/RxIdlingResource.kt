package au.com.timmutton.krazykats.rx

import android.support.test.espresso.IdlingResource
import io.reactivex.functions.Function
import java.util.concurrent.locks.ReentrantReadWriteLock

class RxIdlingResource : IdlingResource, Function<Runnable, Runnable> {
    companion object {
        private val RESOURCE_NAME: String = IdlingResource::javaClass.name
        private val IDLING_STATE_LOCK: ReentrantReadWriteLock = ReentrantReadWriteLock()
    }

    // Guarded by IDLING_STATE_LOCK
    private var taskCount: Int = 0

    // Guarded by IDLING_STATE_LOCK
    private var transitionCallback: IdlingResource.ResourceCallback? = null

    override fun getName(): String {
        return RESOURCE_NAME
    }

    override fun isIdleNow(): Boolean {
        IDLING_STATE_LOCK.readLock().lock()
        val result = taskCount == 0
        IDLING_STATE_LOCK.readLock().unlock()

        return result
    }

    override fun registerIdleTransitionCallback(callback: IdlingResource.ResourceCallback) {
        IDLING_STATE_LOCK.writeLock().lock()
        this.transitionCallback = callback
        IDLING_STATE_LOCK.writeLock().unlock()
    }

    override fun apply(runnable: Runnable): Runnable = Runnable {
        IDLING_STATE_LOCK.writeLock().lock()
        taskCount++
        IDLING_STATE_LOCK.writeLock().unlock()

        try {
            runnable.run()
        } finally {
            IDLING_STATE_LOCK.writeLock().lock();

            try {
                taskCount--

                if (taskCount == 0 && transitionCallback != null) {
                    transitionCallback!!.onTransitionToIdle()
                }
            } finally {
                IDLING_STATE_LOCK.writeLock().unlock()
            }
        }
    }
}