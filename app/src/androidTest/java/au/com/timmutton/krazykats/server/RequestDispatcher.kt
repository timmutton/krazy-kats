package au.com.timmutton.krazykats.server

import okhttp3.mockwebserver.Dispatcher
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.RecordedRequest

class RequestDispatcher: Dispatcher() {
    private val mockServerResponses = hashMapOf<String, Response>()

    @Throws(InterruptedException::class)
    override fun dispatch(recordedRequest: RecordedRequest): MockResponse {
        val response = mockServerResponses[recordedRequest.path.replaceFirst("/", "").split("?").first()]
        return response?.let {
            MockResponse().apply {
                it.headers.forEach {
                    setHeader(it.key, it.value)
                }
                it.body?.let { setBody(it) }
                setResponseCode(it.code)
            }
        } ?: MockResponse().apply {
            setResponseCode(500)
        }
    }

    fun enqueueResponse(path: String, body: Response) {
        mockServerResponses.put(path, body)
    }

    fun reset() {
        mockServerResponses.clear()
    }

    data class Response(val code: Int = 200, val headers: Map<String, String> = emptyMap(), val body: String? = null)
}