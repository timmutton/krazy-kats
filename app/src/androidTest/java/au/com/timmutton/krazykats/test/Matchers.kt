package au.com.timmutton.krazykats.test

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ListView
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.TypeSafeMatcher

fun isListNotEmpty(): Matcher<in View> = object : TypeSafeMatcher<View>() {
    override fun matchesSafely(view: View): Boolean {
        val count = (view as? ListView)?.count ?: ((view as? RecyclerView)?.childCount ?: -1)
        return count > 0
    }

    override fun describeTo(description: Description) {
        description.appendText("ListView should have items")
    }
}