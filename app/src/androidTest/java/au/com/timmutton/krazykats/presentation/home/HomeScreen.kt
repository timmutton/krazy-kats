package au.com.timmutton.krazykats.presentation.home

import android.content.Intent
import android.support.test.InstrumentationRegistry
import android.support.test.espresso.Espresso
import android.support.test.espresso.action.ViewActions
import android.support.test.espresso.assertion.ViewAssertions
import android.support.test.espresso.matcher.ViewMatchers
import au.com.timmutton.krazykats.R
import au.com.timmutton.krazykats.server.RequestDispatcher
import au.com.timmutton.krazykats.test.TestRunner
import au.com.timmutton.krazykats.test.isListNotEmpty

object HomeScreen {
    fun given(init: HomeScreen.Given.() -> Unit) {
        init.invoke(HomeScreen.Given)
    }

    fun whenever(init: HomeScreen.Whenever.() -> Unit) {
        init.invoke(HomeScreen.Whenever)
    }

    fun then(init: HomeScreen.Then.() -> Unit) {
        init.invoke(HomeScreen.Then)
    }

    object Given {
        fun iAmOnHomeScreen() {
            val startIntent = Intent(Intent.ACTION_MAIN)
            startIntent.setClassName(InstrumentationRegistry.getTargetContext().packageName, HomeViewImpl::class.java.name)
            startIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)

            InstrumentationRegistry.getInstrumentation().startActivitySync(startIntent)
        }

        fun iHaveNetworkAccess() {
            TestRunner.dispatcher.enqueueResponse("people.json",
                RequestDispatcher.Response(body = """[
                    {"name":"Bob","gender":"Male","age":23,"pets":[{"name":"Garfield","type":"Cat"},{"name":"Fido","type":"Dog"}]},
                    {"name":"Jennifer","gender":"Female","age":18,"pets":[{"name":"Garfield","type":"Cat"}]},
                    {"name":"Steve","gender":"Male","age":45,"pets":null},
                    {"name":"Fred","gender":"Male","age":40,"pets":[{"name":"Tom","type":"Cat"},{"name":"Max","type":"Cat"},{"name":"Sam","type":"Dog"},{"name":"Jim","type":"Cat"}]},
                    {"name":"Samantha","gender":"Female","age":40,"pets":[{"name":"Tabby","type":"Cat"}]},
                    {"name":"Alice","gender":"Female","age":64,"pets":[{"name":"Simba","type":"Cat"},{"name":"Nemo","type":"Fish"}]}]"""))
        }

        fun iDoNotHaveNetworkAccess() {
            TestRunner.dispatcher.enqueueResponse("people.json", RequestDispatcher.Response(500))
        }
    }

    object Whenever {
        fun iRetryGettingSchedule() {
            Espresso.onView(ViewMatchers.withText(R.string.error_retry)).perform(ViewActions.click())
        }

        fun iHaveNetworkAccess() {
            Given.iHaveNetworkAccess()
        }
    }

    object Then {
        fun catsAreDisplayed() {
            Espresso.onView(ViewMatchers.withId(R.id.catList)).check(ViewAssertions.matches(isListNotEmpty()))
        }

        fun errorIsDisplayed() {
            Espresso.onView(ViewMatchers.withText(R.string.error_description))
                .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        }
    }
}