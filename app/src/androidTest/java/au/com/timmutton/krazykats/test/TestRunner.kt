package au.com.timmutton.krazykats.test

import android.app.Application
import android.content.Context
import android.os.Bundle
import android.os.StrictMode
import android.support.test.espresso.Espresso
import android.support.test.runner.AndroidJUnitRunner
import au.com.timmutton.krazykats.TestKrazyKatsApplication
import au.com.timmutton.krazykats.rx.RxIdlingResource
import au.com.timmutton.krazykats.server.RequestDispatcher
import io.reactivex.plugins.RxJavaPlugins
import okhttp3.mockwebserver.MockWebServer
import timber.log.Timber
import java.io.IOException

class TestRunner: AndroidJUnitRunner() {
    private val MOCK_SERVER_PORT = 8080

    companion object {
        lateinit var server: MockWebServer
        lateinit var dispatcher: RequestDispatcher
    }

    @Throws(InstantiationException::class, IllegalAccessException::class, ClassNotFoundException::class)
    override fun newApplication(cl: ClassLoader, className: String, context: Context): Application {
        return super.newApplication(cl, TestKrazyKatsApplication::class.java.name, context)
    }

    override fun onCreate(arguments: Bundle) {
        super.onCreate(arguments)

        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)

        server = MockWebServer()
        dispatcher = RequestDispatcher()

        server.setDispatcher(dispatcher)
        try {
            server.start(MOCK_SERVER_PORT)
        } catch (e: IOException) {
            Timber.e(e)
        }

        RxJavaPlugins.reset()
        val rxIdlingResource = RxIdlingResource()
        Espresso.registerIdlingResources(rxIdlingResource)
        RxJavaPlugins.setScheduleHandler(rxIdlingResource)
    }

    override fun onDestroy() {
        try {
            server.shutdown()
        } catch (e: IOException) {
            Timber.e(e)
        }

        super.onDestroy()
    }
}