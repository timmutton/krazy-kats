package au.com.timmutton.krazykats.domain

import au.com.timmutton.krazykats.data.owner.OwnerRepository
import au.com.timmutton.krazykats.data.owner.model.Gender
import au.com.timmutton.krazykats.data.owner.model.Owner
import au.com.timmutton.krazykats.data.owner.model.Pet
import au.com.timmutton.krazykats.data.owner.model.Species
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.mockito.Mockito.`when` as on

class RetrieveCatsUseCaseImplTest {
    @Mock
    lateinit var ownerRepository: OwnerRepository

    @InjectMocks
    lateinit var retrieveCatsUseCaseImpl: RetrieveCatsUseCaseImpl

    @Before
    fun init() {
        MockitoAnnotations.initMocks(this)
    }

    @Test
    fun givenLoadAll_onSuccess_returnAll() {
        // Arrange
        val owners = listOf(Owner("John", Gender.MALE, 26, listOf(Pet("Mittens", Species.CAT))))
        on(ownerRepository.getAll()).thenReturn(Single.just(owners))

        // Act
        val observable = retrieveCatsUseCaseImpl.retrieveCats().test().await()

        // Assert
        observable.assertNoErrors()
            .assertValue(mapOf(Gender.MALE to listOf("Mittens")))
    }

    @Test
    fun givenLoadAll_onError_returnError() {
        // Arrange
        val error = Throwable()
        on(ownerRepository.getAll()).thenReturn(Single.error(error))

        // Act
        val observable = retrieveCatsUseCaseImpl.retrieveCats().test().await()

        // Assert
        observable.assertError(error)
    }
}