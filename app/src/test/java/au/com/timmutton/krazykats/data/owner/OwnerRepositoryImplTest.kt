package au.com.timmutton.krazykats.data.owner

import au.com.timmutton.krazykats.data.owner.model.Owner
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.mockito.Mockito.`when` as on

class OwnerRepositoryImplTest {
    @Mock
    lateinit var ownerApi: OwnerRepositoryImpl.OwnerApi

    @InjectMocks
    lateinit var ownerRepository: OwnerRepositoryImpl

    @Before
    fun init() {
        MockitoAnnotations.initMocks(this)
    }

    @Test
    fun givenLoadAll_onSuccess_returnAll() {
        // Arrange
        val owners = emptyList<Owner>()
        on(ownerApi.getAll()).thenReturn(Single.just(owners))

        // Act
        val observable = ownerRepository.getAll().test().await()

        // Assert
        observable.assertNoErrors()
            .assertValue(owners)
    }

    @Test
    fun givenLoadAll_onError_returnError() {
        // Arrange
        val error = Throwable()
        on(ownerApi.getAll()).thenReturn(Single.error(error))

        // Act
        val observable = ownerRepository.getAll().test().await()

        // Assert
        observable.assertError(error)
    }
}