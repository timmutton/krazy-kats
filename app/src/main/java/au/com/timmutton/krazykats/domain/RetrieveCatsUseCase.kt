package au.com.timmutton.krazykats.domain

import au.com.timmutton.krazykats.data.owner.OwnerRepository
import au.com.timmutton.krazykats.data.owner.model.Gender
import au.com.timmutton.krazykats.data.owner.model.Species
import io.reactivex.Single

interface RetrieveCatsUseCase {
    fun retrieveCats(): Single<Map<Gender, List<String>>>
}

class RetrieveCatsUseCaseImpl(private val ownerRepository: OwnerRepository): RetrieveCatsUseCase {
    override fun retrieveCats(): Single<Map<Gender, List<String>>> =
            ownerRepository.getAll().map {
                it.filter { it.pets != null }
                    .flatMap { owner -> owner.pets!!.map { pet -> owner.gender to pet } }
                    .filter { it.second.species == Species.CAT }
                    .groupBy({ it.first }, { it.second.name })
                    .mapValues { it.value.sorted() }
            }
}