package au.com.timmutton.krazykats.data.owner

import au.com.timmutton.krazykats.data.owner.model.Owner
import io.reactivex.Single
import retrofit2.http.GET

interface OwnerRepository {
    fun getAll(): Single<List<Owner>>
}

class OwnerRepositoryImpl(private val ownerApi: OwnerApi) : OwnerRepository {
    override fun getAll(): Single<List<Owner>> = ownerApi.getAll()

    interface OwnerApi {
        @GET("people.json")
        fun getAll(): Single<List<Owner>>
    }
}