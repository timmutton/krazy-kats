package au.com.timmutton.krazykats.injection.module

import au.com.timmutton.krazykats.data.owner.OwnerRepository
import au.com.timmutton.krazykats.data.owner.OwnerRepositoryImpl
import au.com.timmutton.krazykats.injection.annotation.PerInstance
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

@Module
object RepositoryModule {
    @Provides
    @JvmStatic
    @PerInstance
    fun provideOwnerRepository(retrofit: Retrofit): OwnerRepository =
        OwnerRepositoryImpl(retrofit.create(OwnerRepositoryImpl.OwnerApi::class.java))
}