package au.com.timmutton.krazykats.presentation.common.extensions

import android.support.annotation.LayoutRes
import android.view.LayoutInflater
import android.view.ViewGroup

fun ViewGroup.inflate(@LayoutRes res: Int, attachToSelf: Boolean = true) =
        LayoutInflater.from(context).inflate(res, this, attachToSelf)!!