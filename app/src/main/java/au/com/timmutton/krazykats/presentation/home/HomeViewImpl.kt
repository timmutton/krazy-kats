package au.com.timmutton.krazykats.presentation.home

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import au.com.timmutton.krazykats.KrazyKatsApplication
import au.com.timmutton.krazykats.R
import au.com.timmutton.krazykats.data.owner.model.Gender
import au.com.timmutton.krazykats.presentation.common.extensions.toBundle
import au.com.timmutton.krazykats.presentation.common.extensions.toMap
import kotlinx.android.synthetic.main.activity_home.*
import javax.inject.Inject

interface HomeView {
    fun showCats(cats: Map<Gender, List<String>>)
    fun hideCats()

    fun showError(error: Throwable)
    fun hideError()

    fun showLoading()
    fun hideLoading()
}

class HomeViewImpl : AppCompatActivity(), HomeView {
    @Inject
    lateinit var presenter: HomePresenter

    val adapter = CatAdapter()

    // region Lifecycle

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        (application as KrazyKatsApplication).getInjector(this, null).inject(this)

        catList.adapter = adapter
        catList.layoutManager = LinearLayoutManager(this)

        val arguments = intent.extras ?: Bundle()
        savedInstanceState?.let {
            arguments.putAll(it)
        }

        presenter.onCreate(arguments.toMap())
    }

    override fun onStart() {
        super.onStart()

        refreshContainer.setOnRefreshListener {
            presenter.onReload()
        }

        errorButton.setOnClickListener {
            presenter.onReload()
        }

        presenter.onStart()
    }

    override fun onStop() {
        super.onStop()

        presenter.onStop()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        outState.putAll(presenter.getInstanceState().toBundle())
    }

    // endregion

    // region View

    override fun showCats(cats: Map<Gender, List<String>>) {
        catList.visibility = View.VISIBLE

        adapter.setItems(cats)
    }

    override fun hideCats() {
        catList.visibility = View.GONE
    }

    override fun showError(error: Throwable) {
        errorContainer.visibility = View.VISIBLE
    }

    override fun hideError() {
        errorContainer.visibility = View.GONE
    }

    override fun showLoading() {
        refreshContainer.isRefreshing = true
    }

    override fun hideLoading() {
        refreshContainer.isRefreshing = false
    }

    // endregion
}