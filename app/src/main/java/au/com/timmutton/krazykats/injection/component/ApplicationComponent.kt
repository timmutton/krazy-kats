package au.com.timmutton.krazykats.injection.component

import android.content.Context
import dagger.BindsInstance
import dagger.Component
import javax.inject.Named
import javax.inject.Singleton

@Singleton
@Component
interface ApplicationComponent {
    companion object {
        const val BASE_URL_KEY = "BASE_URL"
    }

    fun provideContext(): Context

    @Named(BASE_URL_KEY)
    fun providesBaseUrl(): String

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun applicationContext(applicationContext: Context): Builder

        @BindsInstance
        fun baseUrl(@Named(BASE_URL_KEY) baseUrl: String): Builder

        fun build(): ApplicationComponent
    }
}