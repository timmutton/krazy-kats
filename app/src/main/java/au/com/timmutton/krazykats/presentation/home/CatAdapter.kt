package au.com.timmutton.krazykats.presentation.home

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import au.com.timmutton.krazykats.R
import au.com.timmutton.krazykats.data.owner.model.Gender
import au.com.timmutton.krazykats.presentation.common.extensions.inflate

class CatAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    companion object {
        private const val ITEM_TYPE_HEADING = 0
        private const val ITEM_TYPE_NAME = 1
    }

    private var items: MutableList<Any> = mutableListOf()

    fun setItems(items: Map<Gender, List<String>>) {
        this.items.clear()
        this.items.addAll(items.flatMap { listOf(it.key, *it.value.toTypedArray()) })
        notifyDataSetChanged()
    }
    // TODO: actual data types
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
        when (viewType) {
            ITEM_TYPE_HEADING -> Heading(parent.inflate(R.layout.item_heading, false))
            else -> Name(parent.inflate(android.R.layout.simple_list_item_1, false))
        }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder.itemView as TextView).text = when(holder) {
            is Heading -> (items[position] as Gender).readable
            else -> items[position] as String
        }
    }

    override fun getItemViewType(position: Int): Int = when (items[position]) {
        is Gender -> ITEM_TYPE_HEADING
        else -> ITEM_TYPE_NAME
    }

    override fun getItemCount(): Int = items.count()

    internal class Heading(item: View) : RecyclerView.ViewHolder(item)
    internal class Name(item: View) : RecyclerView.ViewHolder(item)
}