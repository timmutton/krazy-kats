package au.com.timmutton.krazykats.presentation.common.extensions

import android.os.Bundle
import android.os.Parcelable
import java.io.Serializable

@Suppress("UNCHECKED_CAST")
fun <V> Map<String, V>.toBundle(): Bundle = Bundle().apply {
    filterValues { it != null }.forEach { (key, value) ->
        when (value) {
            is Byte -> putByte(key, value)
            is ByteArray -> putByteArray(key, value)
            is Int -> putInt(key, value)
            is IntArray -> putIntArray(key, value)
            is Double -> putDouble(key, value)
            is DoubleArray -> putDoubleArray(key, value)
            is Long -> putLong(key, value)
            is LongArray -> putLongArray(key, value)
            is Float -> putFloat(key, value)
            is FloatArray -> putFloatArray(key, value)
            is Short -> putShort(key, value)
            is ShortArray -> putShortArray(key, value)
            is Boolean -> putBoolean(key, value)
            is BooleanArray -> putBooleanArray(key, value)
            is Char -> putChar(key, value)
            is CharArray -> putCharArray(key, value)
            is CharSequence -> putCharSequence(key, value)
            is Parcelable -> putParcelable(key, value)
            is Serializable -> putSerializable(key, value)
            is Array<*> -> when {
                value.isArrayOf<CharSequence>() -> putCharSequenceArray(key, value as Array<CharSequence>)
                value.isArrayOf<String>() -> putStringArray(key, value as Array<String>)
                value.isArrayOf<Parcelable>() -> putParcelableArray(key, value as Array<Parcelable>)
                else -> throw IllegalArgumentException("$value is of a type that is not currently supported")
            }
            else -> throw IllegalArgumentException("$value is of a type that is not currently supported")
        }
    }
}

fun Bundle.toMap(): Map<String, Any?> = hashMapOf<String, Any?>().apply {
    keySet().forEach {
        put(it, this@toMap.get(it))
    }
}