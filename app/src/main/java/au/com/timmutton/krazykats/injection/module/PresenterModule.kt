package au.com.timmutton.krazykats.injection.module

import au.com.timmutton.krazykats.domain.RetrieveCatsUseCase
import au.com.timmutton.krazykats.injection.annotation.PerInstance
import au.com.timmutton.krazykats.presentation.home.HomePresenter
import au.com.timmutton.krazykats.presentation.home.HomePresenterImpl
import dagger.Module
import dagger.Provides

@Module
class PresenterModule(val view: Any? = null, val router: Any? = null) {
    @Provides
    @PerInstance
    fun provideHomePresenter(retrieveCatsUseCase: RetrieveCatsUseCase): HomePresenter =
        HomePresenterImpl(getContract(view), retrieveCatsUseCase)

    private inline fun <reified T> getContract(item: Any?): T {
        if(item != null && item is T) {
            return item
        } else {
            throw IllegalArgumentException("Required contract for presenter not found")
        }
    }
}