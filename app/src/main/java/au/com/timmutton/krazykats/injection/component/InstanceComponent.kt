package au.com.timmutton.krazykats.injection.component

import au.com.timmutton.krazykats.injection.annotation.PerInstance
import au.com.timmutton.krazykats.injection.module.NetworkModule
import au.com.timmutton.krazykats.injection.module.PresenterModule
import au.com.timmutton.krazykats.injection.module.RepositoryModule
import au.com.timmutton.krazykats.injection.module.UseCaseModule
import au.com.timmutton.krazykats.presentation.home.HomeViewImpl
import dagger.Component

@PerInstance
@Component(dependencies = [ApplicationComponent::class], modules = [NetworkModule::class,
    RepositoryModule::class, UseCaseModule::class, PresenterModule::class])
interface InstanceComponent {
    fun inject(view: HomeViewImpl)
}