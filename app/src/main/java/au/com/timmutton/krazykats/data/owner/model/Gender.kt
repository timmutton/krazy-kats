package au.com.timmutton.krazykats.data.owner.model

import au.com.timmutton.krazykats.common.DontObfuscate

@DontObfuscate
enum class Gender(val readable: String) {
    MALE("Male"),
    FEMALE("Female"),
    OTHER("Other");

    override fun toString(): String = readable

    companion object {
        fun fromString(readable: String): Gender? = Gender.values().find { it.readable == readable }
    }
}