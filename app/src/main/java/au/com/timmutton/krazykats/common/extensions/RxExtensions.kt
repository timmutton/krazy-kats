package au.com.timmutton.krazykats.common.extensions

import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

fun <T> Single<T>.observe(onError: ((Throwable) -> Unit)? = null, onSuccess: ((T) -> Unit)? = null): Disposable {
    return subscribe({
        onSuccess?.invoke(it)
    }, {
        onError?.invoke(it)
    })
}

fun <T> Observable<T>.observe(onError: ((Throwable) -> Unit)? = null, onComplete: (() -> Unit)? = null, onNext: ((T) -> Unit)? = null): Disposable {
    return subscribe({
        onNext?.invoke(it)
    }, {
        onError?.invoke(it)
    }, {
        onComplete?.invoke()
    })
}

fun Completable.observe(onError: ((Throwable) -> Unit)? = null, onComplete: (() -> Unit)? = null): Disposable {
    return subscribe({
        onComplete?.invoke()
    }, {
        onError?.invoke(it)
    })
}

fun <T> Maybe<T>.observe(onError: ((Throwable) -> Unit)? = null, onComplete: (() -> Unit)? = null, onSuccess: ((T) -> Unit)? = null): Disposable {
    return subscribe({
        onSuccess?.invoke(it)
    }, {
        onError?.invoke(it)
    }, {
        onComplete?.invoke()
    })
}

fun Disposable.addTo(compositeDisposable: CompositeDisposable) {
    compositeDisposable.add(this)
}
