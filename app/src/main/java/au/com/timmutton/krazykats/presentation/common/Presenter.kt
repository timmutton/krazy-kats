package au.com.timmutton.krazykats.presentation.common

interface Presenter {
    fun onCreate(arguments: Map<String, Any?>) {}
    fun onStart() {}
    fun onStop() {}

    fun getInstanceState(): Map<String, Any?>
}