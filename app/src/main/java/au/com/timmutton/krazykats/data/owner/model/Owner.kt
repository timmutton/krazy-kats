package au.com.timmutton.krazykats.data.owner.model

import au.com.timmutton.krazykats.common.DontObfuscate

@DontObfuscate
data class Owner(
    val name: String,
    val gender: Gender,
    val age: Int,
    val pets: List<Pet>?
)