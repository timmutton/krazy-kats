package au.com.timmutton.krazykats.injection.module

import au.com.timmutton.krazykats.BuildConfig
import au.com.timmutton.krazykats.data.owner.model.Gender
import au.com.timmutton.krazykats.data.owner.model.Species
import au.com.timmutton.krazykats.injection.annotation.PerInstance
import au.com.timmutton.krazykats.injection.component.ApplicationComponent
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.JsonReader
import com.squareup.moshi.JsonWriter
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import dagger.Module
import dagger.Provides
import io.reactivex.schedulers.Schedulers
import okhttp3.ConnectionSpec
import okhttp3.OkHttpClient
import okhttp3.TlsVersion
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import timber.log.Timber
import javax.inject.Named

@Module
object NetworkModule {
    @Provides
    @JvmStatic
    @PerInstance
    fun providesRetrofit(@Named(ApplicationComponent.BASE_URL_KEY) baseUrl: String): Retrofit = Retrofit.Builder().apply {
        baseUrl(baseUrl)

        addConverterFactory(MoshiConverterFactory.create(Moshi.Builder()
            .add(Gender::class.java, object: JsonAdapter<Gender>() {
                override fun fromJson(reader: JsonReader): Gender =
                    (reader.readJsonValue() as? String)?.let { Gender.fromString(it) } ?: Gender.OTHER

                override fun toJson(writer: JsonWriter, value: Gender?) {
                    value?.let { writer.value(it.toString()) }
                }
            } )
            .add(Species::class.java, object: JsonAdapter<Species>() {
                override fun fromJson(reader: JsonReader): Species =
                    (reader.readJsonValue() as? String)?.let { Species.fromString(it) } ?: Species.OTHER

                override fun toJson(writer: JsonWriter, value: Species?) {
                    value?.let { writer.value(it.toString()) }
                }
            })
            .add(KotlinJsonAdapterFactory())
            .build()))

        addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))

        val client = OkHttpClient().newBuilder().apply {
            if (BuildConfig.DEBUG) {
                val logger = HttpLoggingInterceptor(HttpLoggingInterceptor.Logger { message -> Timber.i(message) })
                logger.level = HttpLoggingInterceptor.Level.BASIC
                addNetworkInterceptor(logger)
            }

            if(BuildConfig.BUILD_TYPE == "release"){
                connectionSpecs(listOf(ConnectionSpec.Builder(ConnectionSpec.MODERN_TLS)
                        .tlsVersions(TlsVersion.TLS_1_3, TlsVersion.TLS_1_2)
                        .build()))
            } else {

            }
        }.build()

        client(client)
    }.build()
}