package au.com.timmutton.krazykats

import android.app.Application
import au.com.timmutton.krazykats.injection.component.ApplicationComponent
import au.com.timmutton.krazykats.injection.component.DaggerApplicationComponent
import au.com.timmutton.krazykats.injection.component.DaggerInstanceComponent
import au.com.timmutton.krazykats.injection.component.InstanceComponent
import au.com.timmutton.krazykats.injection.module.PresenterModule
import timber.log.Timber

open class KrazyKatsApplication: Application() {
    open val API_BASE_URL = "https://agl-developer-test.azurewebsites.net/"

    private val applicationComponent: ApplicationComponent by lazy {
        DaggerApplicationComponent.builder().apply {
            applicationContext(this@KrazyKatsApplication)
            baseUrl(API_BASE_URL)
        }.build()
    }

    override fun onCreate() {
        super.onCreate()

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }

    fun getInjector(view: Any? = null, router: Any? = null): InstanceComponent = DaggerInstanceComponent.builder().apply {
        applicationComponent(applicationComponent)
        presenterModule(PresenterModule(view, router))
    }.build()
}