package au.com.timmutton.krazykats.presentation.home

import au.com.timmutton.krazykats.common.extensions.observe
import au.com.timmutton.krazykats.data.owner.model.Gender
import au.com.timmutton.krazykats.domain.RetrieveCatsUseCase
import au.com.timmutton.krazykats.presentation.common.Presenter
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import timber.log.Timber

interface HomePresenter : Presenter {
    fun onReload()
}

class HomePresenterImpl(
    private val view: HomeView,
    private val retrieveCatsUseCase: RetrieveCatsUseCase
) : HomePresenter {
    companion object {
        private const val GENDERS_KEY = "genders"
    }

    private var catsObservable: Single<Map<Gender, List<String>>>? = null
    private var catsSubscription: Disposable? = null

    private var results: Map<Gender, List<String>>? = null

    @Suppress("UNCHECKED_CAST")
    override fun onCreate(arguments: Map<String, Any?>) {
        results = (arguments[GENDERS_KEY] as? Array<String>)?.map {
            Gender.fromString(it)!! to (arguments[it] as Array<String>).toList()
        }?.toMap()

        if(results == null) {
            catsObservable = createRetrieveCatsObservable()
        } else {
            view.showCats(results!!)
        }
    }

    override fun onStart() {
        catsSubscription = createRetrieveCatsSubscription()
    }

    override fun onStop() {
        catsSubscription?.dispose()
        catsSubscription = null
    }

    // Supporting non-binary or unknown genders makes this quite complicated
    override fun getInstanceState(): Map<String, Any?> = results?.let { results ->
            mapOf(GENDERS_KEY to results.keys.map { it.readable },
                *results.map { entry -> entry.key.readable to entry.value.toTypedArray() }.toTypedArray())
        } ?: emptyMap()

    override fun onReload() {
        catsObservable = createRetrieveCatsObservable()
        catsSubscription = createRetrieveCatsSubscription()
    }

    private fun createRetrieveCatsObservable() = retrieveCatsUseCase.retrieveCats()
        .observeOn(AndroidSchedulers.mainThread())
        .cache()

    private fun createRetrieveCatsSubscription() = catsObservable
        ?.doOnSubscribe { view.showLoading() }
        ?.doOnSuccess { results = it }
        ?.doFinally { view.hideLoading() }
        ?.observe(onError = {
            Timber.e(it)

            view.hideCats()
            view.showError(it)
        }, onSuccess = {
            Timber.d(it.toString())

            view.hideError()
            view.showCats(it)
        })
}