package au.com.timmutton.krazykats.injection.module

import au.com.timmutton.krazykats.data.owner.OwnerRepository
import au.com.timmutton.krazykats.domain.RetrieveCatsUseCase
import au.com.timmutton.krazykats.domain.RetrieveCatsUseCaseImpl
import au.com.timmutton.krazykats.injection.annotation.PerInstance
import dagger.Module
import dagger.Provides

@Module
object UseCaseModule {
    @Provides
    @JvmStatic
    @PerInstance
    fun provideRetieveCatUseCase(ownerRepository: OwnerRepository): RetrieveCatsUseCase =
        RetrieveCatsUseCaseImpl(ownerRepository)
}