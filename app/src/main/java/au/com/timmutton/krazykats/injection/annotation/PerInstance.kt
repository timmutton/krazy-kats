package au.com.timmutton.krazykats.injection.annotation

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class PerInstance