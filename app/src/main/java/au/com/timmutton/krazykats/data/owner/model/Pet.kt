package au.com.timmutton.krazykats.data.owner.model

import au.com.timmutton.krazykats.common.DontObfuscate
import com.squareup.moshi.Json

@DontObfuscate
data class Pet(
    val name: String,
    @Json(name = "type") val species: Species
)